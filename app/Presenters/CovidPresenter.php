<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;

final class CovidPresenter extends Nette\Application\UI\Presenter {

    public function actionCovidAkce() {
        $this->template->jmeno = "Covid JanFiala";
    }

      public function createComponentCovidAkce(string $name): \Nette\ComponentModel\IComponent {
        return new \App\Model\Forms\CovidAkce ($this, $name);
    }

}