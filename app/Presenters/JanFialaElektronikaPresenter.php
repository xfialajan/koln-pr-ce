<?php

declare(strict_types = 1);

namespace App\Presenters;

use Nette;

final class JanFialaElektronikaPresenter extends Nette\Application\UI\Presenter {

    public $nazev_stranky;

    public function actionJanFialaEdit() {
        $this->template->edit = "JanFialaEdit";
    }

    public function actionJanFialaDelete() {

        $this->template->delete = "JanFialaDelete";
    }

    public function actionJanFialaAdd() {

        $this->template->add = "JanFialaAdd";
    }




 public function createComponentJanFialaRegistrace(string $name){
        return new \app\models\Forms\JanFialaRegistrace ($this, $name);
}
}