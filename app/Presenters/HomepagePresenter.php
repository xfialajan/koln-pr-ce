<?php

declare(strict_types=1);

namespace App\Presenters;

use UserFiala;
use Nette;

final class HomepagePresenter extends Nette\Application\UI\Presenter {

    public function actionDefault() {

        $pin1 = new \UtilsJanFiala();

        echo $pin1->getPinJanFiala();
        echo '<br>';
        echo $pin1->getPinJanFiala();
        echo '<br>';
        echo $pin1->getPinJanFiala();
        echo '<br>';
        echo $pin1->getPinJanFiala();
        echo '<br>';
        echo $pin1->getPinJanFiala();
        echo '<br>';
        echo '<br>';

        $Jan = new \UtilsJanFiala();
        $Jan->setUsernameFiala("Jan");

        echo $Jan->getStreetFiala();
        echo $Jan->getZipFiala();
        echo $Jan->getInvoiceIdFiala();
        echo $Jan->getLoginCountFiala();
        echo $Jan->getBornDateFiala();

        dump($Jan);
    }

}