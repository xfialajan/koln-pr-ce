<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;

final class JanFialaUstrednaPresenter extends Nette\Application\UI\Presenter {

    public function actionNastaveniJanFiala() {
        $this->template->jmeno = "Covid Jan Fiala";
    }

      public function createComponentNastaveniJanFiala(string $name): \Nette\ComponentModel\IComponent {
        return new \App\Model\Forms\nastaveniJanFiala ($this, $name);
    }

}