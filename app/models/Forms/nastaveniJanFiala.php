<?php

namespace App\Model\Forms;

use Nette\Application\UI\Form;


 
class nastaveniJanFiala extends Form {

    public function __construct()
    
    {
        
    
       $this->addText("Jmeno","Jméno a příjmení");
        $this->addText("0102040218","Rodné číslo");
        $this->addTextArea("priznaky","Příznaky");
        $this->addSelect("Datum","Čas objednání",array("17:15","17:30","18:00"));
        $this->addUpload("obrazek","obrazek");
        $this->addRadioList("Test","Test",array("antigenní","kemr","total skem"));
        $this->addRadioList("pohlavi","Pohlaví",array("Muž","Žena","Jiné"));
        $this->addInteger("vek","věk");
        $this->addCheckbox("potvrzeni","Potvrzuji přečtení pravidel");
         



  $this->addPassword('password', 'Heslo')
       ->addRule(Form::EMAIL, 'Heslo musi mit alespon 6 znaku', 6); 
       $this->addSubmit ("odeslat","odeslat");
       $this->onSuccess[] = [$this,'formSubmitted'];


}

   public function formSubmitted () {
       $data = $this->getValues ();
       dump ($data);
       die;
   }

}
